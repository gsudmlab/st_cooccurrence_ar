# -*- coding: utf-8 -*-
"""
Created on Sun Sep 15 15:55:53 2019

@author: Sumi
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Sep  5 22:12:16 2019

@author: Sumi
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 15:22:51 2019

@author: Sumi
"""

#!/usr/bin/env python
# coding: utf-8



import sys
import time
from datetime import datetime
import pandas as pd
import geopandas as gpd
import numpy as np
import glob as glb
from shapely.geometry import box
from shapely.geometry import Polygon
from shapely.geometry import Point
import shapely.wkt
from datetimerange import DateTimeRange
import datetime as dt
import astropy.units as u
from astropy.coordinates import SkyCoord
from sunpy.coordinates import frames

#for windows path r'C:\Users\Sumi\Desktop\noaa_harp_test_windows\sharp_geo'
#for ubuntun path '/home/xmcai8/sharp_ar'
sharp_out_folder = r'E:\diff_140_distance_INPUT'

sharp_files = glb.glob(sharp_out_folder + "/*.csv")


#for windows path r'C:\Users\Sumi\Desktop\noaa_harp_test_windows\noaa_geo_precise_id.csv'
#for ubuntun path '/home/xmcai8/noaa_ar/noaa_geo_precise_id.csv'
noaa_path = r'E:\Noaa-ar\noaa_ars_plages_n_modified.csv'

#for windows path r'C:\Users\Sumi\Desktop\noaa_harp_test_windows\noaa_id_to_harp.csv'
#for ubuntun path '/home/xmcai8/noaa_harp/noaa_id_to_harp.csv'
noaa_harp_path = r'C:\Users\Sumi\Desktop\noaa_more_new\noaa_diff\diff_140_dist_need_n.csv'

#for windows path r'C:\\Users\\Sumi\\Desktop\\noaa_harp_test_windows\\noaa_sharp_out'
#for ubuntun path '/home/xmcai8/sharp_noaa/'
export_foler = r'E:\\diff_140_distance_OUTPUT\\'

st_form = '%Y-%m-%d %H:%M:%S'


#get sharp file as dictionary
def get_sharp(file_path):
        
    data_sharp ={}
    
    for filename in file_path:

        #ubuntun harp_name = filename.split("/")[-1].rstrip('.csv')
        harp_name = filename.split("\\")[-1].rstrip('.csv')
        
        df = pd.read_csv(filename, header=0,parse_dates=True,date_parser=pd.to_datetime,index_col='Timestamp')
        
        df['hgs_poly'] = df['hgs_poly'].apply(lambda row: shapely.wkt.loads(row) if type(row) is str else None )
        
        data_sharp[int(harp_name)] = df
        
    
    return data_sharp



#get noaa file as dataframe
def get_noaa(file_path):
    

    noaa_ar = pd.read_csv(file_path)
    noaa_ar = noaa_ar.rename(columns={'Unnamed: 0': 'id'})
    noaa_ar = noaa_ar.set_index("id")
    noaa_ar['year'] = noaa_ar['year'].astype(str)
    noaa_ar['month'] = noaa_ar['month'].astype(str)
    noaa_ar['day'] = noaa_ar['day'].astype(str)
    noaa_ar['ar_time'] = pd.to_datetime(noaa_ar[['year', 'month', 'day']].apply(lambda x: '-'.join(x), axis=1))
    #noaa_ar['Datetime_st_time'] = noaa_ar['ar_time'].apply(lambda row: row - dt.timedelta(hours = 12))
    #noaa_ar['Datetime_ed_time'] = noaa_ar['ar_time'].apply(lambda row: row + dt.timedelta(hours = 12))
    noaa_ar['longitude'] = noaa_ar['central_meridian_dist']
    noaa_ar['latitude'] = noaa_ar['latitude']
    
    
    df = noaa_ar[['ar_time','noaa_ar_no','longitude','latitude']]
    
    

    #df['ar_time'] = pd.to_datetime(df['ar_time'])
    
    
    
    #df['geo_point'] = df['geo_point'].apply(shapely.wkt.loads)
    
    print('finish loading Noaa')
    
    return df



#get noaa->harp match dictionary
def get_noaa_harp_map(file_path):
    
    df = pd.read_csv(file_path)
    
    df['diff_noaa'] = df['diff_noaa'].apply(lambda row: list(map(int,row.strip('[]').split(','))) if type(row) is str else None)
    
    df_map = {row['HARPNUM']:row['diff_noaa'] for idx,row in df.iterrows()}
    
    return df_map


#lon,lat --x,y
def coord_transformer(lon,lat,edate):
#     lon = x['LON_MIN']
#     lat = x['LAT_MIN']
#     edate = x['Timestamp']
    c = SkyCoord(lon*u.deg, lat*u.deg, frame=frames.HeliographicStonyhurst, obstime=edate)
    c_hpc = c.transform_to(frames.Helioprojective)
    return c_hpc.Tx.arcsec,c_hpc.Ty.arcsec #Tx,Ty


def calculate_lon_delta(lat_degree, day):
    alpha = 14.11
    beta = -1.7
    gamma = -2.35

    velocity_in_deg = alpha + beta * (np.sin(np.deg2rad(lat_degree))) ** 2 + gamma * (np.sin(np.deg2rad(lat_degree))) ** 4
    delta_lon = velocity_in_deg * day
    return delta_lon


def new_lon(o_lon,o_lat,edate,b_time):
    
    t_diff_dat = (edate - b_time)/ pd.Timedelta('1 day')

# ar_lat = ar_record[0]['latitude']
#         ar_lon = ar_record[0]['central_meridian_dist']

#         t_diff_day = ar_record[0]['diff'] 
        #         print t_diff_day
    new_ar_lon = o_lon + calculate_lon_delta(o_lat, -t_diff_dat)
    
    if new_ar_lon > 90:
        new_ar_lon = 90
        
    elif new_ar_lon < -90:
        new_ar_lon = -90
        
    return new_ar_lon



def matching_process():
    
    sharp_f_dic = get_sharp(sharp_files)
    
    noaa_df = get_noaa(noaa_path)
    
    #'diff_noaa_in_all NOAA_ARS'
    
    noaa_sharp_dic = get_noaa_harp_map(noaa_harp_path)
    
    
    #check each harp_num
    for key_harp in noaa_sharp_dic:
        
        if noaa_sharp_dic[key_harp] !=None:
            
            print("processing:" + str(key_harp))
            
            for idx,noaa_no in enumerate(noaa_sharp_dic[key_harp]):
                
#                 print(noaa_no)
                
                noaa_idx = noaa_df[noaa_df['noaa_ar_no']==noaa_no].index.values
                
                for n in noaa_idx:
                    
                    print(n)
                    
                    st_time = (noaa_df.loc[n,'ar_time'] - dt.timedelta(hours = 12)).strftime(st_form)
                    
                    ed_time = (noaa_df.loc[n,'ar_time'] + dt.timedelta(hours = 11,minutes =48)).strftime(st_form)
                    
                    
                    #retrieve geometry not none index
                    geo_not_null_idx = sharp_f_dic[key_harp]['hgs_poly'].notnull()
                    
                    noaa_harp_idx = sharp_f_dic[key_harp][geo_not_null_idx].loc[st_time : ed_time].index.values
                    
#                     print(noaa_harp_idx)
                    
                    if len(noaa_harp_idx)>0:  #noaa number has time intersection with harp
                        
#                         print(n)
                        
                        for b in noaa_harp_idx:
                            
                            #print(b)
                            
#                             print(key_harp)
                            
                            
                            poly_sharp = sharp_f_dic[key_harp].loc[b,'hgs_poly']#
                            
                            noaa_lon = noaa_df.loc[n,'longitude']
                        
                            noaa_lat = noaa_df.loc[n,'latitude']
                        
                            noaa_date = noaa_df.loc[n,'ar_time']
                        
                            noaa_new_lon = new_lon(noaa_lon,noaa_lat,noaa_date,b)
                            
                            point_noaa = Point(noaa_new_lon, noaa_lat)
                            
                            noaa_info = str(n)+':'+ str(noaa_df.loc[n,'noaa_ar_no'])
                            
                            
                            
                            if point_noaa.within(poly_sharp):
                            
                                noaa_dist = str(0)
                        
                            else:
                            
                                noaa_dist = str(poly_sharp.exterior.distance(point_noaa))
                            
                            #print(noaa_dist)
                            
                            
                            
                            
                             
                                
                                
                                
                                
                                
                                
                                
                                
                                
                            if 'noaa_num' not in sharp_f_dic[key_harp].columns:
                                    
                                sharp_f_dic[key_harp]['noaa_num'] = ''
                                        
                                sharp_f_dic[key_harp]['noaa_distance'] = ''
                                                                      
                                    
                                sharp_f_dic[key_harp].loc[b,'noaa_num'] = noaa_info
                                    
                                sharp_f_dic[key_harp].loc[b,'noaa_distance'] = noaa_dist
                                    
                                    

                                        
                                
                            elif len(sharp_f_dic[key_harp].loc[b,'noaa_num']) ==0:
                                    
                                sharp_f_dic[key_harp].loc[b,'noaa_num'] = noaa_info
                                    
                                sharp_f_dic[key_harp].loc[b,'noaa_distance'] = noaa_dist
                                    
                                
                                    
                                
                                
                            else:
                                    
                                sharp_f_dic[key_harp].loc[b,'noaa_num'] = sharp_f_dic[key_harp].loc[b,'noaa_num'] +";"+ noaa_info
                                    
                                sharp_f_dic[key_harp].loc[b,'noaa_distance'] = sharp_f_dic[key_harp].loc[b,'noaa_distance'] +";"+ noaa_dist
                                    
                                
                                
                    
                    
                
            
        
        
        
    for key_sharp in sharp_f_dic:
                
        f_name = str(key_sharp)
                
        sharp_f_dic[key_sharp].to_csv('{0}{1}.csv'.format(export_foler,f_name),index = True)


matching_process()






