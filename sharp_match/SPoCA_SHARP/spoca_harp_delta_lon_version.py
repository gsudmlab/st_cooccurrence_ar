# -*- coding: utf-8 -*-
"""
Created on Sat Jul 20 12:14:41 2019

@author: Sumi
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 17:28:01 2019

@author: Sumi
"""

#!/usr/bin/env python
# coding: utf-8



#import sys
# import time
from datetime import datetime
import pandas as pd
#import geopandas as gpd
import numpy as np
import glob as glb
from shapely.geometry import box
from shapely.geometry import Polygon
from shapely.geometry import Point
import shapely.wkt
from datetimerange import DateTimeRange
import datetime as dt
import astropy.units as u
from astropy.coordinates import SkyCoord
from sunpy.coordinates import frames

#for windows path r'C:\Users\Sumi\Desktop\spoca_harp_test_windows\sharp_geo'
#for ubuntun path '/home/xmcai8/sharp_ar'
sharp_out_folder = r'E:\Sharp_hgs_poly'

sharp_files = glb.glob(sharp_out_folder + "/*.csv")


#for windows path r'C:\Users\Sumi\Desktop\spoca_harp_test_windows\spoca_geo_precise_id.csv'
#for ubuntun path '/home/xmcai8/spoca_ar/spoca_geo_precise_id.csv'
spoca_path = r'C:\Users\Sumi\Desktop\SpoCA_harp_test\Spoca_w_id_all.csv'

#for windows path r'C:\Users\Sumi\Desktop\spoca_harp_test_windows\spoca_id_to_harp.csv'
#for ubuntun path '/home/xmcai8/spoca_harp/spoca_id_to_harp.csv'
spoca_harp_path = r'C:\Users\Sumi\Desktop\SpoCA_harp_test\spocaa_id_harp.csv'

#for windows path r'C:\\Users\\Sumi\\Desktop\\spoca_harp_test_windows\\spoca_sharp_out'
#for ubuntun path '/home/xmcai8/sharp_spoca/'
export_foler = r'E:\\SPoCA_delta_lon_poly_sharp_output\\'

st_form = '%Y-%m-%d %H:%M:%S'


#get sharp file as dictionary
def get_sharp(file_path):
        
    data_sharp ={}
    
    for filename in file_path:

        #ubuntun harp_name = filename.split("/")[-1].rstrip('.csv')
        harp_name = filename.split("\\")[-1].rstrip('.csv')
        
        df = pd.read_csv(filename, header=0,parse_dates=True,date_parser=pd.to_datetime,index_col='Timestamp')
        
        df['hgs_poly'] = df['hgs_poly'].apply(lambda row: shapely.wkt.loads(row) if type(row) is str else None )
        
        data_sharp[int(harp_name)] = df
        
    
    return data_sharp



#get spoca file as dataframe
def get_spoca(file_path):
    
    df = pd.read_csv(file_path)
    
    df['event_date'] = pd.to_datetime(df['event_date'])
    
    df['event_starttime'] = pd.to_datetime(df['event_starttime'])
    
    df['event_endtime'] = pd.to_datetime(df['event_endtime'])
    
    df['hgs_bbox'] = df['hgs_bbox'].apply(shapely.wkt.loads)
    
    #df['spoca_id'] = df['spoca_id'].astype('int64')
    
    print('finish loading')
    
    return df



#get spoca->harp match dictionary
def get_spoca_harp_map(file_path):
    
    df = pd.read_csv(file_path)
    
    #transform str to int of list
    df['harp_no'] = df['harp_no'].apply(lambda row: list(map(int,row.strip('[]').split(','))) if type(row) is str else None)
    
    #'spoca_id' in spocaa_id_harp.csv index
    df_map = {row['spoca_id']:row['harp_no'] for idx,row in df.iterrows()}
    
    return df_map



#lon,lat --x,y
def coord_transformer(lon,lat,edate):
#     lon = x['LON_MIN']
#     lat = x['LAT_MIN']
#     edate = x['Timestamp']
    c = SkyCoord(lon*u.deg, lat*u.deg, frame=frames.HeliographicStonyhurst, obstime=edate)
    c_hpc = c.transform_to(frames.Helioprojective)
    return c_hpc.Tx.arcsec,c_hpc.Ty.arcsec #Tx,Ty


def calculate_lon_delta(lat_degree, day):
    alpha = 14.11
    beta = -1.7
    gamma = -2.35

    velocity_in_deg = alpha + beta * (np.sin(np.deg2rad(lat_degree))) ** 2 + gamma * (np.sin(np.deg2rad(lat_degree))) ** 4
    delta_lon = velocity_in_deg * day
    return delta_lon


def new_lon(o_lon,o_lat,edate,b_time):
    
    t_diff_dat = (edate - b_time)/ pd.Timedelta('1 day')

# ar_lat = ar_record[0]['latitude']
#         ar_lon = ar_record[0]['central_meridian_dist']

#         t_diff_day = ar_record[0]['diff'] 
        #         print t_diff_day
    new_ar_lon = o_lon + calculate_lon_delta(o_lat, -t_diff_dat)
    
    if new_ar_lon > 90:
        new_ar_lon = 90
        
    elif new_ar_lon < -90:
        new_ar_lon = -90
        
    return new_ar_lon








def matching_process():
    
    sharp_f_dic = get_sharp(sharp_files)
    
    print('finish loading sharp')
    
    spoca_df = get_spoca(spoca_path)
    
    print('finish loading spoca')
    
    spoca_sharp_dic = get_spoca_harp_map(spoca_harp_path)
    
    print('finish loading spoca-harp map')
    
    
    #check each spoca_num
    for key_spoca in spoca_sharp_dic:
        
        if spoca_sharp_dic[key_spoca] !=None:
            
            print("processing:" + str(key_spoca))
            
            st_time = spoca_df.loc[key_spoca,'event_starttime'].strftime(st_form)
        
            ed_time = spoca_df.loc[key_spoca,'event_endtime'].strftime(st_form)
        
            #poly_spoca = spoca_df.loc[key_spoca,'hpc_bbox']
        
            #spoca_id:spoca_frm_number
            spoca_info = str(key_spoca)+':'+ str(spoca_df.loc[key_spoca,'spoca_id'])
        
         
        
        
            for sharp_idx,sh_no in enumerate(spoca_sharp_dic[key_spoca]):
                
                

                
                
                #retrieve geometry not none index
                geo_not_null_idx = sharp_f_dic[sh_no]['hgs_poly'].notnull()
                    
                    #filter the time
                spoca_harp_idx = sharp_f_dic[sh_no][geo_not_null_idx].loc[st_time : ed_time].index.values
                    
                    
                    #time range for spoca-harp
                if len(spoca_harp_idx)>0:
                        
                    for b in spoca_harp_idx:
                        
                        #p_lon_min = spoca_df.loc[key_spoca,'hgs_bbox'].bounds[0]
                        #p_lat_min = spoca_df.loc[key_spoca,'hgs_bbox'].bounds[1]
                        #p_lon_max = spoca_df.loc[key_spoca,'hgs_bbox'].bounds[2]
                        #p_lat_max = spoca_df.loc[key_spoca,'hgs_bbox'].bounds[3]
                        
                        p0_lon = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[0][0]
                        p1_lon = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[1][0]
                        p2_lon = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[2][0]
                        p3_lon = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[3][0]

                        p0_lat = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[0][1]
                        p1_lat = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[1][1]
                        p2_lat = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[2][1]
                        p3_lat = list(spoca_df.loc[key_spoca,'hgs_bbox'].exterior.coords)[3][1]
                        
                        spoca_date = spoca_df.loc[key_spoca,'event_starttime']
                        
                        
                        
                        p0_n_lon = new_lon(p0_lon,p0_lat,spoca_date,b)
                        p1_n_lon = new_lon(p1_lon,p1_lat,spoca_date,b)
                        p2_n_lon = new_lon(p2_lon,p2_lat,spoca_date,b)
                        p3_n_lon = new_lon(p3_lon,p3_lat,spoca_date,b)

                        
                        
                        


                        #p_lon_min_new_lon = new_lon(p_lon_min,p_lat_min,spoca_date,b) #new min lontitude
                        #p_lon_max_new_lon = new_lon(p_lon_max,p_lat_max,spoca_date,b) #new max lontitude
        
        
        
                        #poly_spoca = box(p_lon_min_new_lon,p_lat_min,p_lon_max_new_lon,p_lat_max)
                        poly_spoca = Polygon([(p0_n_lon, p0_lat), (p1_n_lon, p1_lat), (p2_n_lon, p2_lat),(p3_n_lon, p3_lat),(p0_n_lon, p0_lat)])
                        
                                
                        poly_sharp = sharp_f_dic[sh_no].loc[b,'hgs_poly']
                            
        #                     #if sharp geometry is none
        #                     if poly_sharp !=None:
                                
                                #spoca is inside sharp box
                        if poly_spoca.intersects(poly_sharp):
                                    
                            if 'spoca_frm_id' not in sharp_f_dic[sh_no].columns:
                                        
                                sharp_f_dic[sh_no]['spoca_frm_id'] = ''
                                        
                                sharp_f_dic[sh_no].loc[b,'spoca_frm_id'] = spoca_info
                                        
                                    
                            elif len(sharp_f_dic[sh_no].loc[b,'spoca_frm_id']) ==0:
                                        
                                sharp_f_dic[sh_no].loc[b,'spoca_frm_id'] = spoca_info
                                    
                                    
                            else:
                                    
                                sharp_f_dic[sh_no].loc[b,'spoca_frm_id'] = sharp_f_dic[sh_no].loc[b,'spoca_frm_id'] +";"+ spoca_info
                                    

                
    print('finish matching')
        
        
        
    for key_sharp in sharp_f_dic:
                
        f_name = str(key_sharp)
                
        sharp_f_dic[key_sharp].to_csv('{0}{1}.csv'.format(export_foler,f_name),index = True)


matching_process()






