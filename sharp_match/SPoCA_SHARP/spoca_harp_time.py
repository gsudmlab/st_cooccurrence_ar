#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import sys
import time
from datetime import datetime
import pandas as pd
import numpy as np
from datetimerange import DateTimeRange


spoca_path = 'Spoca_w_id_precise_all.csv'
harp_path = 'harp_time_range.csv'
out_path = 'spocaa_id_harp.csv'





def get_spoca_file(file_path):
    
    df = pd.read_csv(file_path)[['event_date','event_starttime','event_endtime']]
    
    df['event_date'] = pd.to_datetime(df['event_date'])
    df['event_starttime'] = pd.to_datetime(df['event_starttime'])
    df['event_endtime'] = pd.to_datetime(df['event_endtime'])
    
    
    return df


def get_harp_time_file(file_path):
    
    df = pd.read_csv(file_path)
    
    df['st_time'] = df['date_time_range'].apply(lambda row:row.split(' - ')[0])
    df['ed_time'] = df['date_time_range'].apply(lambda row:row.split(' - ')[1])
    df['Date_time_range'] = df.apply(lambda row: DateTimeRange(row['st_time'],row['ed_time']),axis = 1)
    
    return df



def matching_time_spoca_harp():
    
    
    spoca_time_df = get_spoca_file(spoca_path)
    
    print(spoca_time_df.info())
    
    spoca_time_lst = [DateTimeRange(row['event_starttime'],row['event_endtime']) for idx,row in spoca_time_df.iterrows()]
    
    print(len(spoca_time_lst))
    
    harp_time_df = get_harp_time_file(harp_path)
    
    print(harp_time_df.info())
    
    harp_time_dic = {row['harp_num']:row['Date_time_range'] for idx,row in harp_time_df.iterrows()}
    
    print(len(harp_time_dic))
    
    
    spoca_harp_map = {}
    
    for idx_spoca,time_elem in enumerate(spoca_time_lst):
        
        print('start: '+str(idx_spoca))
        
        spoca_harp_map[idx_spoca] = [key for key in harp_time_dic if time_elem.is_intersection(harp_time_dic[key])]
        
        print('finish: '+str(idx_spoca))
        
    
    spoca_harp_map_df = pd.DataFrame(spoca_harp_map.items(), columns=['spoca_id','harp_no'])
    
    spoca_harp_map_df['harp_no'] = spoca_harp_map_df['harp_no'].apply(lambda row: None if len(row)==0 else row)
    
    spoca_harp_map_df.to_csv(out_path,index=False)
    
    print('finish')
    
    
    
    

if __name__ == "__main__":
    matching_time_spoca_harp()

