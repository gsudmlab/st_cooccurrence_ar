# -*- coding: utf-8 -*-
"""
This script is for downloading the geometric data of 
SPoCA active region from HECK
attributes inluding both hpc and hgs attributes
"""

import sys
import time
from datetime import datetime
from urllib3.exceptions import HTTPError

import pandas as pd
import sunpy.instr.goes
from dateutil.relativedelta import relativedelta
from sunpy.net import hek
from sunpy.time import TimeRange
from datetimerange import DateTimeRange

startdate = datetime(2013, 12, 30)
enddate = datetime(2018, 12, 31)

# DAY_DELTA = 20
#ofile_suffix = '_2012.csv'


def get_monthly_range(start_date):
    return DateTimeRange(start_date, start_date + relativedelta(months=+1))


def get_daily_range(start_date):
    return DateTimeRange(start_date, start_date + relativedelta(days=+1))


def download_activeregion(start_date, end_date, ofile):
    client = hek.HEKClient()
    event_type = 'AR'
    FRM = 'SPoCA'
    # FRM = 'SSW Latest Events'
    # INSTRUMENT = 'AIA'

    failed_download_ranges = []
    listofresults = []
    columns = ['event_date', 'event_starttime', 'event_endtime', 'hgs_coord', 'hgs_bbox','hpc_coord','hpc_bbox','hpc_x','hpc_y',
               'ar_spotarearaw', 'ar_spotarearawuncert', 'ar_spotarearawunit', 'ar_spotarearepr', 
               'ar_spotarearepruncert','ar_spotareareprunit','sharp_noaa_ars','AR_NOAANUM',
               'obs_instrument', 'SOL_standard','frm_name','frm_specificid']

    temp_start = start_date
    while temp_start < end_date:
        tr = get_daily_range(temp_start)

        print ('Downloading activeregion from ', tr.start_datetime, 'to', tr.end_datetime)
        # temp = None
        results = []

        try:
            results = client.search(hek.attrs.Time(tr.start_datetime, tr.end_datetime), hek.attrs.EventType(event_type),
                                    hek.attrs.FRM.Name == FRM,
                                    hek.attrs.FRM.HumanFlag == 'false'
                                    # hek.attrs.OBS.Instrument == INSTRUMENT
                                    )
        except HTTPError as http_error:
            failed_download_ranges.append(tr.start_datetime)
            print ('I could not download for ', tr.start_datetime, 'to', tr.end_datetime)

        print('\tDownloaded {0} activeregion'.format(len(results)))
        for result in results:
            # print result
            # print result.keys()
            listofresults.append([result['event_starttime'].split('T')[0], result['event_starttime'],
                                   result['event_endtime'],  result['hgs_coord'], result['hgs_bbox'],
                                   result['hpc_coord'],result['hpc_bbox'],result['hpc_x'],result['hpc_y'],
                                  result['ar_spotarearaw'], result['ar_spotarearawuncert'], 
                                  result['ar_spotarearawunit'], result['ar_spotarearepr'], 
                                  result['ar_spotarearepruncert'],result['ar_spotareareprunit'],
                                  result['sharp_noaa_ars'], result['ar_noaanum'], result['obs_instrument'], result['SOL_standard'],
                                  result['frm_name'],result['frm_specificid']])
        temp_start = tr.end_datetime
    print('Length of total results: ', len(listofresults))
    print('Failed download ranges\n', str(failed_download_ranges))
    df = pd.DataFrame(listofresults, columns=columns)
    df.to_csv(ofile, sep='\t', index=False)


def run_main():
    # start = time.time()
    # download_goes_flares(startdate, enddate, './datain/goesFlares' + ofile_suffix)
    # end = time.time()
    #
    # print("Finished downloading GOES flares in: %.2f minutes." % ((end - start) / 60.0))

    # start = time.time()
    # download_aia_flares(startdate, enddate, './datain/aiaFlares' + ofile_suffix)
    # end = time.time()
    #
    # print("Finished downloading AIA flares in: %.2f minutes." % ((end - start) / 60.0))

    start = time.time()
    download_activeregion(startdate, enddate, './AR_Spoca_2')
    end = time.time()

    print("Finished downloading AIA no frm flares in: %.2f minutes." % ((end - start) / 60.0))


if __name__ == "__main__":
    run_main()