# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 16:55:58 2019

@author: Sumi
"""

import sys
import time
import datetime as dt
import pandas as pd
import numpy as np
from datetimerange import DateTimeRange

noaa_path = r'E:\Noaa-ar\noaa_ars_plages_n.csv'
harp_time_path = r'C:\Users\Sumi\Desktop\spoca_harp_time\harp_time_range.csv'
noaa_harp_id_out_path = 'C:\\Users\\Sumi\\Desktop\\noaa_more_new\\noaa_id_to_harp_n.csv'

#get noaa_file
def get_noaa_ar_df(file_path):
    noaa_ar = pd.read_csv(file_path)
    noaa_ar = noaa_ar.rename(columns={'Unnamed: 0': 'id'})
    noaa_ar = noaa_ar.set_index("id")
    noaa_ar['year'] = noaa_ar['year'].astype(str)
    noaa_ar['month'] = noaa_ar['month'].astype(str)
    noaa_ar['day'] = noaa_ar['day'].astype(str)
    noaa_ar['ar_time'] = pd.to_datetime(noaa_ar[['year', 'month', 'day']].apply(lambda x: '-'.join(x), axis=1))
    noaa_ar['Datetime_st_time'] = noaa_ar['ar_time'].apply(lambda row: row - dt.timedelta(hours = 12))
    noaa_ar['Datetime_ed_time'] = noaa_ar['ar_time'].apply(lambda row: row + dt.timedelta(hours = 12))
    noaa_ar['longitude'] = noaa_ar['central_meridian_dist']
    noaa_ar['latitude'] = noaa_ar['latitude']
    
    
    df = noaa_ar[['ar_time','noaa_ar_no','longitude','latitude','Datetime_st_time','Datetime_ed_time']]
    
    
    return df


def get_harp_time_file(file_path):
    
    df = pd.read_csv(file_path)
    
    df['st_time'] = df['date_time_range'].apply(lambda row:row.split(' - ')[0])
    df['ed_time'] = df['date_time_range'].apply(lambda row:row.split(' - ')[1])
    df['Date_time_range'] = df.apply(lambda row: DateTimeRange(row['st_time'],row['ed_time']),axis = 1)
    
    return df
    


def matching_time_noaa_harp():
    
    
    noaa_time_df = get_noaa_ar_df(noaa_path)
    
    print(noaa_time_df.info())
    
    noaa_time_lst = [DateTimeRange(row['Datetime_st_time'],row['Datetime_ed_time']) for idx,row in noaa_time_df.iterrows()]
    
    print(len(noaa_time_lst))
    
    harp_time_df = get_harp_time_file(harp_time_path)
    
    print(harp_time_df.info())
    
    harp_time_dic = {row['harp_num']:row['Date_time_range'] for idx,row in harp_time_df.iterrows()}
    
    print(len(harp_time_dic))
    
    
    noaa_harp_map = {}
    
    for idx_noaa,time_elem in enumerate(noaa_time_lst):
        
        print('start: '+str(idx_noaa))
        
        noaa_harp_map[idx_noaa] = [key for key in harp_time_dic if time_elem.is_intersection(harp_time_dic[key])]
        
        print('finish: '+str(idx_noaa))
        
    
    noaa_harp_map_df = pd.DataFrame(list(noaa_harp_map.items()), columns=['noaa_id','harp_no'])
    
    noaa_harp_map_df['harp_no'] = noaa_harp_map_df['harp_no'].apply(lambda row: None if len(row)==0 else row)
    
    noaa_harp_map_df.to_csv(noaa_harp_id_out_path,index=False)
    
    print('finish')
    


matching_time_noaa_harp()