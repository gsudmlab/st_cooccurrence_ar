# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 15:22:51 2019

@author: Sumi
"""

#!/usr/bin/env python
# coding: utf-8



import sys
import time
from datetime import datetime
import pandas as pd
import geopandas as gpd
import numpy as np
import glob as glb
from shapely.geometry import box
from shapely.geometry import Polygon
from shapely.geometry import Point
import shapely.wkt
from datetimerange import DateTimeRange
import datetime as dt
import astropy.units as u
from astropy.coordinates import SkyCoord
from sunpy.coordinates import frames

#for windows path r'C:\Users\Sumi\Desktop\noaa_harp_test_windows\sharp_geo'
#for ubuntun path '/home/xmcai8/sharp_ar'
sharp_out_folder = r'E:\Sharp_hgs_poly'

sharp_files = glb.glob(sharp_out_folder + "/*.csv")


#for windows path r'C:\Users\Sumi\Desktop\noaa_harp_test_windows\noaa_geo_precise_id.csv'
#for ubuntun path '/home/xmcai8/noaa_ar/noaa_geo_precise_id.csv'
noaa_path = r'C:\Users\Sumi\Desktop\noaa_more_new\noaa_ar_more.csv'

#for windows path r'C:\Users\Sumi\Desktop\noaa_harp_test_windows\noaa_id_to_harp.csv'
#for ubuntun path '/home/xmcai8/noaa_harp/noaa_id_to_harp.csv'
noaa_harp_path = r'C:\Users\Sumi\Desktop\noaa_more_new\noaa_id_to_harp.csv'

#for windows path r'C:\\Users\\Sumi\\Desktop\\noaa_harp_test_windows\\noaa_sharp_out'
#for ubuntun path '/home/xmcai8/sharp_noaa/'
export_foler = r'E:\\Noaa_MORE_TIME_OUTPUT\\'

st_form = '%Y-%m-%d %H:%M:%S'


#get sharp file as dictionary
def get_sharp(file_path):
        
    data_sharp ={}
    
    for filename in file_path:

        #ubuntun harp_name = filename.split("/")[-1].rstrip('.csv')
        harp_name = filename.split("\\")[-1].rstrip('.csv')
        
        df = pd.read_csv(filename, header=0,parse_dates=True,date_parser=pd.to_datetime,index_col='Timestamp')
        
        df['hgs_poly'] = df['hgs_poly'].apply(lambda row: shapely.wkt.loads(row) if type(row) is str else None )
        
        data_sharp[int(harp_name)] = df
        
    
    return data_sharp



#get noaa file as dataframe
def get_noaa(file_path):
    
    df = pd.read_csv(file_path)
    

    df['ar_time'] = pd.to_datetime(df['ar_time'])
    
    
    
    #df['geo_point'] = df['geo_point'].apply(shapely.wkt.loads)
    
    print('finish loading Noaa')
    
    return df



#get noaa->harp match dictionary
def get_noaa_harp_map(file_path):
    
    df = pd.read_csv(file_path)
    
    df['harp_no'] = df['harp_no'].apply(lambda row: list(map(int,row.strip('[]').split(','))) if type(row) is str else None)
    
    df_map = {row['noaa_id']:row['harp_no'] for idx,row in df.iterrows()}
    
    return df_map


#lon,lat --x,y
def coord_transformer(lon,lat,edate):
#     lon = x['LON_MIN']
#     lat = x['LAT_MIN']
#     edate = x['Timestamp']
    c = SkyCoord(lon*u.deg, lat*u.deg, frame=frames.HeliographicStonyhurst, obstime=edate)
    c_hpc = c.transform_to(frames.Helioprojective)
    return c_hpc.Tx.arcsec,c_hpc.Ty.arcsec #Tx,Ty


def calculate_lon_delta(lat_degree, day):
    alpha = 14.11
    beta = -1.7
    gamma = -2.35

    velocity_in_deg = alpha + beta * (np.sin(np.deg2rad(lat_degree))) ** 2 + gamma * (np.sin(np.deg2rad(lat_degree))) ** 4
    delta_lon = velocity_in_deg * day
    return delta_lon


def new_lon(o_lon,o_lat,edate,b_time):
    
    t_diff_dat = (edate - b_time)/ pd.Timedelta('1 day')

# ar_lat = ar_record[0]['latitude']
#         ar_lon = ar_record[0]['central_meridian_dist']

#         t_diff_day = ar_record[0]['diff'] 
        #         print t_diff_day
    new_ar_lon = o_lon + calculate_lon_delta(o_lat, -t_diff_dat)
    
    if new_ar_lon > 90:
        new_ar_lon = 90
        
    elif new_ar_lon < -90:
        new_ar_lon = -90
        
    return new_ar_lon



def matching_process():
    
    sharp_f_dic = get_sharp(sharp_files)
    
    noaa_df = get_noaa(noaa_path)
    
    noaa_sharp_dic = get_noaa_harp_map(noaa_harp_path)
    
    
    #check each noaa_num
    for key_noaa in noaa_sharp_dic:
        
        if noaa_sharp_dic[key_noaa] !=None:
            
            print("processing:" + str(key_noaa))
            
            st_time = (noaa_df.loc[key_noaa,'ar_time'] - dt.timedelta(hours = 12)).strftime(st_form)
        
            ed_time = (noaa_df.loc[key_noaa,'ar_time'] + dt.timedelta(hours = 12)).strftime(st_form)#,minutes =48
        
            #point_noaa = noaa_df.loc[key_noaa,'geo_point']
        
            #noaa_id:noaa_number
            noaa_info = str(key_noaa)+':'+ str(noaa_df.loc[key_noaa,'noaa_ar_no'])
        
         
        
        
            for sharp_idx,sh_no in enumerate(noaa_sharp_dic[key_noaa]):
                
                

                
                
                #retrieve geometry not none index
                geo_not_null_idx = sharp_f_dic[sh_no]['hgs_poly'].notnull()
                    
                    #filter the time
                noaa_harp_idx = sharp_f_dic[sh_no][geo_not_null_idx].loc[st_time : ed_time].index.values
                    
                    
                    #time range for noaa-harp
                if len(noaa_harp_idx)>0:
                        
                    for b in noaa_harp_idx:
                                
                        poly_sharp = sharp_f_dic[sh_no].loc[b,'hgs_poly']
                        
                        noaa_lon = noaa_df.loc[key_noaa,'longitude']
                        
                        noaa_lat = noaa_df.loc[key_noaa,'latitude']
                        
                        noaa_date = noaa_df.loc[key_noaa,'ar_time']
                        
                        noaa_new_lon = new_lon(noaa_lon,noaa_lat,noaa_date,b)
                        
                        #p_x,p_y = coord_transformer(noaa_new_lon,noaa_lat,noaa_date)
                        
                        point_noaa = Point(noaa_new_lon, noaa_lat)
                        
                        
                        
                            
        #                     #if sharp geometry is none
        #                     if poly_sharp !=None:
                                
                                #noaa is inside sharp box
                        if point_noaa.within(poly_sharp):
                                    
                            if 'noaa_num' not in sharp_f_dic[sh_no].columns:
                                        
                                sharp_f_dic[sh_no]['noaa_num'] = ''
                                        
                                sharp_f_dic[sh_no].loc[b,'noaa_num'] = noaa_info
                                        
                                    
                            elif len(sharp_f_dic[sh_no].loc[b,'noaa_num']) ==0:
                                        
                                sharp_f_dic[sh_no].loc[b,'noaa_num'] = noaa_info
                                    
                                    
                            else:
                                    
                                sharp_f_dic[sh_no].loc[b,'noaa_num'] = sharp_f_dic[sh_no].loc[b,'noaa_num'] +";"+ noaa_info
                                    

                    
                    
                
            
        
        
        
    for key_sharp in sharp_f_dic:
                
        f_name = str(key_sharp)
                
        sharp_f_dic[key_sharp].to_csv('{0}{1}.csv'.format(export_foler,f_name),index = True)


matching_process()






