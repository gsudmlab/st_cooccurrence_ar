# Integrating Solar Active Region using Spatio-temporal Co-occurrence Analysis

This is the project to integrate vector data of solar active region from NOAA, SHARP, SPoCA based on spatial and temporal attributes. It is composed of number of python scripts and analysis


The project is organized as follows:

Step 1:
NOAA AR anomalus detection and analysis
Data: /sharp_match/NOAA_SHARP/NOAA AR/noaa_ars_plages_n.csv

NOAA AR Daily Change folder contains:

1.Provide analysis for detecting and analyze Longititude and Latitude Daily Change of NOAA AR Location

2.Related analysis plot figure for NOAA AR anomalous detection analysis in NOAA AR Daily Change/Noaa_daily_differ_latitude_in_consecutive_days.ipynb


Step 2:

Spatio-temporal matching procedudre between NOAA_SHARP and SPoCA_SHARP

Data:
SHARP headers(available online from JSOC)
NOAA AR: sharp_match/NOAA_SHARP/NOAA AR/noaa_ars_plages_n_modified_updated.csv
SPoCA: sharp_match/SPoCA_SHARP/Spoca_w_id_all.csv 

sharp_match folder contains:

1.Python script of spatio-temporal co-occurrence association procedure between NOAA_SHARP and SPoCA_SHARP
(sharp_match/NOAA_SHARP/noaa_harp_delta_lon_version_n.py
sharp_match/SPoCA_SHARP/spoca_harp_delta_lon_version.py)

2.Both NOAA_SHARP and SPoCA_SHARP provides temporal intersection and spatial intersection python script.
(sharp_match/NOAA_SHARP/noaa_harp_time.py
sharp_match/SPoCA_SHARP/spoca_harp_time.py)

3.Preprocessed vector data of NOAA AR and SPoCA AR(including downloader)


Step 3:
Discrepancy analysis between SHARP_NOAA AR co-occurrence based association and associations provided by JSOC


JSOC association: Discrepancy Analyze/all_harps_with_noaa_ars.txt
First_step co_occurrenc association 
Discrepancy Analyze folder contains: Discrepancy Analyze/First_step_co_occurrence_association.csv

1.Analysis of SHARP_NOAA AR association based on co-occurrence with the association provided by JSOC(SHARP series data source)

2.Python script for calculating the distance between NOAA AR number_HARP number for all difference pairs with JSOC

3.Analysis of NOAA AR, HARP co-existance and co-occurrence rate















